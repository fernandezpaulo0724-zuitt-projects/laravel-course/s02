<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    // public function hello(){
    //     return view('hello')->with('name', 'Homer Simpson');
    // }

    public function hello(){
        $info = array(
            'front_end' => 'Zuitt Coding Bootcamp',
            'topics' => [
                
            ]
        );
        return view('hello')->with($info);
    }

    public function index(){
        $title = 'Welcome to Laravel';
        return view('pages.index')->with('title', $title);
    }
    public function about(){
        return view('pages.about');
    }
    public function services(){
        $data = array(
            'title' => 'Services Page',
            'services' => [
                'Web design', 
                'Development',
                'SEO'
            ]
        );
        return view('pages.services')->with($data);
    }
}
